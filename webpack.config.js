const path = require('path');
const fs = require('fs');

module.exports = {
    entry: './src/main.js',
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            { test: /\.css$/, loader: 'style!css' },
            {
                test: /\.scss$/,
                loaders: ['style-loader', 'raw-loader', 'sass-loader']
            },
            {
                test: /\.js$/,
                include: [
                    // path.resolve(__dirname, 'src'),
                    // path.resolve(__dirname, 'node_modules/phaser'),
                    // fs.realpathSync('./node_modules/phaser')
                ],
                loader: 'babel-loader',
                query: {
                    presets: ['babel-preset-es2015'].map(require.resolve)
                }
            },
            {
                test: /\.(png|jpg|gif|html)$/,
                use: 
                  {
                    loader: 'file-loader',
                    options: {},
                  }
            }
        ]
    },
    resolve: {
        //alias: { phaser: path.resolve(__dirname, 'node_modules/phaser/src/') }
    }
};
