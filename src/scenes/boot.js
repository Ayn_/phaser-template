import Player from '../assets/spritesheets/dude.png'

export default class extends Phaser.Scene {
    constructor () {
        super({ key: 'Boot' })
    }

    preload () {
        this.loadingBar();

        this.load.spritesheet('Player', Player, { frameWidth: 32, frameHeight: 48 });
    }

    create () {
        this.scene.start('Map_1')
    }

    loadingBar() {
        var width = this.cameras.main.width;
        var height = this.cameras.main.height;

        // Create loading box.
        var progressBar = this.add.graphics();
        var progressBox = this.add.graphics();
        progressBox.fillStyle(0x222222, 0.8);
        progressBox.fillRect(width/2-160, height/2-25, 320, 50); // 240 x, 270y
        
        // Genering loading text.
        var loadingText = this.make.text({
            x: width / 2,
            y: height / 2 - 50,
            text: 'Loading...',
            style: {
                font: '20px monospace',
                fill: '#ffffff'
            }
        });

        loadingText.setOrigin(0.5, 0.5);

        // Generate loading boxes.
        var percentText = this.make.text({
            x: width / 2,
            y: height / 2,
            text: '0%',
            style: {
                font: '18px monospace',
                fill: '#ffffff'
            }
        });

        percentText.setOrigin(0.5, 0.5);

        // Generate text to display file name.
        var assetText = this.make.text({
            x: width / 2,
            y: height / 2 + 50,
            text: '',
            style: {
                font: '18px monospace',
                fill: '#ffffff'
            }
        });
        assetText.setOrigin(0.5, 0.5);

        this.load.on('progress', function (value) {
        progressBar.clear();
        progressBar.fillStyle(0xffffff, 1);
        progressBar.fillRect(width/2-160+10, height/2-25+10, 300 * value, 30);
        percentText.setText(parseInt(value * 100) + '%');
        });
                    
        this.load.on('fileprogress', function (file) {
        // can also use file.src - or other elements of the file object.
        assetText.setText('Loading asset: ' + file.key);
        });

        this.load.on('complete', function () {
        progressBar.destroy();
        progressBox.destroy();
        loadingText.destroy();
        percentText.destroy();
        assetText.destroy();
        });
    }
}
  