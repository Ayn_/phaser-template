export default class Player extends Phaser.Physics.Arcade.Sprite {
    constructor(scene, x, y) {
        super(scene, x, y, 'Player')
        
        this.speed = 200

        // enable physics
        scene.physics.world.enable(this)

        // set world bounds
        this.setCollideWorldBounds(true)

        // Create animations
        scene.anims.create({
            key: 'left',
            frames: scene.anims.generateFrameNumbers('Player', { start: 0, end: 3 }),
            frameRate: 10,
            repeat: -1
        });
        
        scene.anims.create({
            key: 'turn',
            frames: [ { key: 'Player', frame: 4 } ],
            frameRate: 20
        });
        
        scene.anims.create({
            key: 'right',
            frames: scene.anims.generateFrameNumbers('Player', { start: 5, end: 8 }),
            frameRate: 10,
            repeat: -1
        });

        this.anims.play('turn')

        // Take cursor keyboard input
        this.cursors = scene.input.keyboard.createCursorKeys()
    
        // add player to the scene
        scene.add.existing(this)
    }

    update() {
        if (this.cursors.left.isDown) {
            this.setVelocityX(-this.speed);
            this.anims.play('left', true);
        } else if (this.cursors.right.isDown) {
            this.setVelocityX(this.speed);
            this.anims.play('right', true);
        } else {
            this.setVelocityX(0);
            this.anims.play('turn');
        }
        
        if (this.cursors.up.isDown) {
            this.setVelocityY(-this.speed*2);
        }
    }
    
}
