// Phaser
import Phaser from 'phaser'

import * as scenes from './scenes'

var game = new Phaser.Game({
    type: Phaser.AUTO,
    parent: 'game', 
    dom: { createContainer: true },
    // backgroundColor: 0x333333,
    width: 800,
    height: 600,
    pixelArt: true,
    roundPixels: true,
    crisp: true,            
    scale: {
        mode: Phaser.Scale.RESIZE,
        autoCenter: Phaser.Scale.CENTER_BOTH
    },
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 300 },
            debug: false
        }
    },    
    scene: Object.values(scenes)
});